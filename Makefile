### DIRS ##################################################################
SRC_DIR = src
OBJ_DIR = obj

### OBJS ##################################################################
QUEUE_OBJS 	= $(OBJ_DIR)/queue.o
HASH_OBJS 	= $(OBJ_DIR)/hashtable.o

OBJS = $(QUEUE_OBJS) $(HASH_OBJS)
TESTEXES = testqueue testhash 

### FLAGS #################################################################
CFLAGS = -Wall -Wextra -pedantic -ansi -g -O
DBGFLAGS = -DDEBUG #-DDEBUGQ #-DDEBUGN #-DDEBUGS #-DDEBUGI
LFLAGS = #-lm

### COMMANDS ##############################################################
CC =  gcc
RM = rm -f
COMPILE = $(CC) $(CFLAGS) $(DBGFLAGS)

### RULES #################################################################

all: $(TESTEXES)

tests: $(TESTEXES)

testqueue: $(SRC_DIR)/testqueue.c $(QUEUE_OBJS)
	$(COMPILE) -o $@ $^ $(LFLAGS)

testhash: $(SRC_DIR)/testhashtable.c $(HASH_OBJS)
	$(COMPILE) -o $@ $^ $(LFLAGS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(COMPILE) -c -o $@ $^

.PHONY: clean
clean:
	$(RM) $(OBJS) $(TESTEXES)
	$(RM) $(OBJ_DIR)/*.o

