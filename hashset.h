/*
 * This lookuptable is specifically for the implementing a* with the aim
 * of solving an n-puzzle.
 */
#ifndef LOOKUPTABLE_H
#define LOOKUPTABLE_H

#include "game.h"

#define TRUE	1
#define SUCCESS	1
#define FALSE	0
#define FAIL	0
#define HT_ERROR 5

typedef struct hashset *hashset_ptr;

/**
 * Allocate the datastructures of a hashset and initialise the parameters.
 *
 * @param[in][out] ht a pointer to a pointer of the newly created hashset.
 */
void hashset_init_defaults(hashset_ptr *hs);


/**
 * Allocate the datastructures of a hashset and initialise the parameters.
 *
 * @param[in][out]	ht			a pointer to a pointer of the newly 
 *								created hashset.
 * @param[in]		init_delta	The initial index into the hashtables 
 *								delta table
 * @param[in]		delta_diff	The increment by which to increase the
 *								delta index which resizing
 *
 */
void hashset_init(hashset_ptr *hs, int init_delta, int delta_diff);

/**
 * Insert a Agame instance into the hashset.
 *
 * @param[in] hs A pointer to the hashset into which to insert.
 *
 * @param[in] game A pointer to the game instance to insert.
 *
 * @return SUCCESS(1) if the game was inserted successfully and
 * FAIL(0) if not.
 */
int hashset_insert(hashset_ptr hs, game_t *game);

/**
 *	Print the underlying hashtable of the hashset. 
 *
 *	@param[in] hs A pointer to the hashset to print.  
 */
void print_hashset(hashset_ptr hs);

/**
 * Free the values in the hashset and the hashset itself. 
 *
 * @param[in] hs The hashset to be free'd.  
 */
void free_hashset(hashset_ptr hs);

#endif
