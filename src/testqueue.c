#include <stdio.h>
#include <stdlib.h>

#include "queue.h"

typedef struct object {
	int number;
} object_t;

/*** Helper Function Prototypes ******************************************/

object_t *new_object(int N);
void free_object(object_t *o);

int cmp_g(void *g1, void *g2);
void free_g(void *n);
void print_g(void *n);


/*** Main Routine ********************************************************/

int main(void)
{
	int i;
	int count = 5;

	object_t *o = NULL;
	object_t *temp = NULL;
	queue_t *q = NULL;

	

	init_queue(&q, cmp_g, free_g);

	if (!q) {
		fprintf(stderr, "Initialising queue failed. Exiting abnormally.\n");
		goto CLEANUP;
	}

	/* Try popping from a queue with nothing in it.  */
	if ((temp = pop_first(q))) {
		printf("This is weird! %p\n", (void *)temp);
	} else {
		printf("\x1b[1;34mNice!\x1b[0m  doesn't break upon poping from empty queue\n");
	}

	/* 
	 * Lets enter some stuff into the queue. 
	 */
	o = new_object(4);
	if (!o) {
		fprintf(stderr, "bad memory allocation. Exiting abnormally.\n");
		goto CLEANUP;
	}
	o->number = 4;

	printf("About to insert first entry\n");
	if (insert_node(q, o)) {
		printf("Just inserted the following object:\n");
		print_g(o);
		printf("\n");
		o = NULL;
	} else {
		printf("Failed to insert object\n");
	}
	

	for (i = 10; i > 0; i--) {
		o = new_object(i);
		if (!o) {
			fprintf(stderr, "bad memory allocation. Exiting abnormally.\n");
			goto CLEANUP;
		}
		insert_node(q, o);
		o = NULL;
	}

	/* test whether emptying the queue works */
	empty_queue(q);

	/* 
	 * now that it is empty, check that popping from the empty queue
	 * doesn't cause problems. 
	 */
	for (i = 0; i < count + 1; i++) {
		if ((o = pop_first(q))) {
			print_g(o);
			printf("\n");
			free_g(o);
		} else {
			printf("seems empty\n\n");
		}
	}

	/* Check whether one can still add stuff to the emptied queue */


	for (i = 0; i < 7; i++) {
		o = new_object(i);
		if (!o) {
			fprintf(stderr, "bad memory allocation. Exiting abnormally.\n");
			goto CLEANUP;
		}
		insert_node(q, o);
		o = NULL;
	}

	for (i = 10; i > 4; i--) {
		o = new_object(i);
		if (!o) {
			fprintf(stderr, "bad memory allocation. Exiting abnormally.\n");
			goto CLEANUP;
		}
		insert_node(q, o);
		o = NULL;
	}

	/* print this queue now */
	print_queue(q, print_g);

CLEANUP:
	printf("cleanup:\n");
	/* Free data structures */
	if (o) {
		free_g((void *)o);
		o = NULL;
	}
	free_queue(q);

	/* no segfaults or early exits */
	printf("exiting without seg\n");
	return 0;
}

/*** Helper Functions ****************************************************/


int cmp_g(void *g1, void *g2)
{
	object_t *a = g1;
	object_t *b = g2;

	return (a->number - b->number);
}

void free_object(object_t *o) 
{
	free(o);
}

void free_g(void *n)
{
	object_t *object = n;

	free_object(object);
}


void print_g(void *n) 
{
	object_t *o = n;
	printf("object: %d\n", o->number);
}


object_t *new_object(int N)
{
	object_t *o = NULL;
	
	o = malloc(sizeof(object_t));

	if (o) {
		o->number = N;
	}
	return o;
}
