#ifndef HASHTABLE_H
#define HASHTABLE_H

#define KEY_PRESENT_IN_TABLE -1
#define SUCCESS 1
#define FAIL 0

typedef struct hashtable *hashtable_p;

/**
 * Initilise the structures required for the hashtable. 
 *
 * @param[in] factor	The fraction at which the table must resize
 * @param[in] hash		The hashing function to use on keys.
 * @param[in] cmp		The comparison function to use when comparing
 *						keys.
 *
 * @return A pointer to a newly allocated hashtable
 */
hashtable_p ht_init(float factor, int init_delta, int delta_diff, 
		unsigned long (*hash)(void *key, unsigned int size), 
		int (*cmp)(void *a, void *b));

/**
 * Attempt to insert a key value pair.
 *
 * @param[in] ht		The table into which to attempt to put the pair.  
 *
 * @param[in] key		The unique key, to be used for hashing and 
 *						comparison with other entries in the table.  
 * @param[in] value		Thevalue of the pair to be put in the table.
 *
 * @return EXIT_SUCCESS (0) if successfully put in the table, 1 if memory
 * problems occur and KEY_PRESENT_IN_TABLE (-1) if the key is already in
 * the table. 
 */
int ht_insert(hashtable_p ht, void *key, void *value);

/**
 * Check to see if a given key value pair is present in the table.
 *
 * @param[in]	ht		The hashtable to look in.  
 * @param[in]	key		The key to use to compare with entries in the 
 *						hashtable.
 * @param[out]	value	A double pointer, where the value of the pair is
 *						stored, if found.
 *
 * @return SUCCESS (1) if found and FAIL (0) if not. 
 */ 
int ht_lookup(hashtable_p ht, void *key, void **value);

/**
 * Free the entries of the hashtable using the provided function pointers,
 * after which the table itself is also free'd.
 *
 * @param[in] ht		The hashtable to free
 * @param[in] freekey	A function pointer to free keys in the table with.
 * @param[in] freeval	A function pointer to free values in the table 
 *						with.
 */
void ht_free(hashtable_p ht, void (*freekey)(void *key),
		void (*freeval)(void *v));

/**
 * Print the hashtable using the provided function pointers. 
 *
 * @param[in] ht			The hashtable to print.
 * @param[in] val_to_str	A function to get a string representation
 *							of the key value pair with.
 */
void print_ht(hashtable_p ht, void (*val_to_str)(void *key, void *val,
			char *buffer));

#endif
